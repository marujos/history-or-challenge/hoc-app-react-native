import { StatusBar } from 'react-native';
import React from 'react';
import { Provider } from 'react-redux';

import Routes from './src/routes';
import { colors } from './src/styles';
import store from './src/store';

export default function App() {
  return (
    <>
      <StatusBar barStyle='light-content' backgroundColor={colors.dark} />
      <Provider store={store}>
        <Routes />
      </Provider>
    </>
  );
}
