const fonts = {
    extra: '32px',
    big: '20px',
    regular: '14px',
    medium: '12px',
    small: '11px',
    tiny: '10px',
};

export default fonts;