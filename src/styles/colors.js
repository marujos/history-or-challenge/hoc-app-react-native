
const colors = {
    background: '#1C1A1C',
    header: '#0D0C0D',
    red: '#C41A13',
    yellow: '#FFAB00',
    dark: '#1C1A1C',
    green: '#129E42',
    pink: '#FF439F'
};

export default colors;