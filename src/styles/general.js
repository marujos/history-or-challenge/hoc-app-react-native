import React from 'react';
import styled from 'styled-components/native';
import { colors, fonts } from './index';

export const Content = styled.View`
    width: 100%;
    padding: 20px;
`;
