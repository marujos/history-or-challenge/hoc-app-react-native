import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Auth from '../pages/Auth';
const Stack = createStackNavigator();

const navigationOptions = ({ title, headerLeft }) => {
    return ({
        headerTitle: title || '',
        headerTransparent: true,
        headerBackTitle: ' ',
        headerTintColor: '#fff',
        headerLeft
    });
};

export default function AuthRoutes() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Auth" component={Auth} options={navigationOptions} />
        </Stack.Navigator>
    );

}
