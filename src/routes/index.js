import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { useSelector } from 'react-redux';
import AuthRoutes from './auth.routes';
import AppRoutes from './app.routes';

const Stack = createStackNavigator();

export default function App() {
    const selector = useSelector(state => state.auth);
    const authenticated = selector.authenticated;
    return (
        <NavigationContainer>
            {authenticated ? <AppRoutes /> : <AuthRoutes />}
        </NavigationContainer>
    );
}