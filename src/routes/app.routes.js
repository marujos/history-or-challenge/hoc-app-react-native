import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Home from '../pages/Home';
const Stack = createStackNavigator();

const navigationOptions = ({ title, headerLeft }) => {
    return ({
        headerTitle: title || '',
        headerTransparent: true,
        headerBackTitle: ' ',
        headerTintColor: '#fff',
        headerLeft
    });
};

export default function AppRoutes() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={Home} options={navigationOptions} />
        </Stack.Navigator>
    );

}
