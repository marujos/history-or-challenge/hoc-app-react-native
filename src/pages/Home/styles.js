import styled from 'styled-components/native';
import { colors, fonts } from '../../styles';

export const Container = styled.View`
    flex:1;
    background: ${colors.dark};
    align-items: center;
    justify-content: center;
`;

export const Card = styled.View`
    margin: 10px 0;
    padding: 30px ;
    background: ${colors.red};
    height: 40px;
    width: 100%;
    align-items: center;
    justify-content: center;
`;

export const CardText = styled.Text`
    color: white;
    font-size: ${fonts.big};
`;

export const AddButton = styled.View`
    background: black;
    height: 70px;
    width: 70px;
    border-radius: 50px;
    align-items: center;
    justify-content: center;
`;