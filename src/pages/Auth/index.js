import React from 'react';
import { useDispatch } from 'react-redux';
import { Creators as AuthActions } from '../../store/ducks/auth';

import { Container, ButtonLogin, TextLogin, BoxLogin, ConteinerLogo, TextInput, SignButton, SignText, BoxImage } from './styles';

export default function Auth({ navigation }) {
    const dispach = useDispatch();

    const login = () => {
        dispach(AuthActions.addAuthenticated({ authenticated: true }));
    };

    return (
        <Container >

            <BoxImage>
                <ConteinerLogo source={require('../../assets/logo.png')} />
            </BoxImage>

            <BoxLogin>

                <TextInput
                    placeholder="E-mail"
                    autoCorrect={false}
                    onChangeText={() => { }}
                />
                <TextInput
                    placeholder="Senha"
                    autoCorrect={false}
                    onChangeText={() => { }}
                />

                <ButtonLogin onPress={login}>
                    <TextLogin>Login</TextLogin>
                </ButtonLogin>

                <SignButton>
                    <SignText>Criar conta</SignText>
                </SignButton>


            </BoxLogin>
        </Container>
    );
}


