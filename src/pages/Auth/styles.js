import styled from 'styled-components/native';
import { colors, fonts } from '../../styles';

export const Container = styled.View`
    flex:1;
    background: ${colors.dark};
`;

export const BoxLogin = styled.View`
    flex: 1;
    align-items:center;
    justify-content: center;
    padding:  10px;
`;

export const ButtonLogin = styled.TouchableOpacity`
    background: ${colors.red};
    width: 100%;
    padding:10px;
    align-items:center;
    border-radius: 10px;
`;

export const TextLogin = styled.Text`
    color: white;
    font-size: ${fonts.big};
    font-weight: bold;
`;

export const TextInput = styled.TextInput`
    background : #FFF;
    width: 100%;
    margin-bottom: 15px;
    color: #222;
    font-size: 17px;
    border-radius: 7px;
    padding: 10px;
`;

export const ConteinerLogo = styled.Image`
`;

export const SignButton = styled.TouchableOpacity`
    margin-top: 10px;
`;

export const SignText = styled.Text`
    color: #FFF;
`;

export const BoxImage = styled.View`
    flex: 1;
    align-items:center;
    justify-content: flex-end;
    
`;